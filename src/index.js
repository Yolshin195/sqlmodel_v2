class Sql {
  constructor() {
    this._sql = '';
  }

  select(...domains) {
    this._sql = 'SELECT'; 

    if(domains.length) {
      this._sql += ` ${domains}`;
    } else {
      this._sql += ' *';
    }

    return this;
  }

  insert() {
    this._sql = 'INSERT';
    return this;
  }

  update() {
    this._sql = 'UPDATE';
    return this;
  }

  delete() {
    this._sql = 'DELETE';
    return this;
  }

  from(attitude) {
    this._sql += ' FROM';
    this._sql += ` ${attitude}`; 
    return this;
  }

  where(filter) {
    this._sql += ' WHERE';
    const where = [];
    for(let key in filter) {
      if(filter[key] instanceof Array) {
        where.push(`${key} in (${filter[key]})`);
        continue;
      }

      where.push(`${key} = ${filter[key]}`);
    }

    this._sql += ` ${where.join(' AND ')}`;
    return this;
  }

  groupBy() {

    return this;
  }

  orderBy() {

    return this;
  }

  toString() {
    return this._sql + ';';
  }
}

const sql = new Sql();
var sqlString = sql
                .select()
                .from('bike_st')
                .where({marka_id: [1,2,4], marka_name: 'honda'})
                .toString();

console.log(sqlString);
